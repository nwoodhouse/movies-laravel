<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
	use Notifiable;

	protected $fillable = [
        'name', 'email', 'password', 'api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

	function greetAndGiveEmail() {
		return "hello " . $this->name . " Your email is " . $this->email;
	}

	function greet() {
		return "hello " . $this->name;
	}

	function editUrl() {
		return route('users', $this->id);
	}


	public function movies() {
    	return $this->belongsToMany(Movie::class);
    }
}
