<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Producer;
use App\User;

class Movie extends Model
{
    protected $appends = ['url'];

	public function producer()
    {
        return $this->belongsTo(Producer::class);
    }

    public function users() {
        return $this->belongsToMany(User::class);
    }

    public function greeting() {
        return "hi";
    }

    public function getUrlAttribute()
    {
        return route('movies.show', $this->id);
    }
}

