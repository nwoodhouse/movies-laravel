<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producer extends Model
{
    
    protected $fillable = [
		'name'
    ];

    public $timestamps = false;


    public function movies()
    {
        return $this->hasMany('App\Movie');
    }
}
