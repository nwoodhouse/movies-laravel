<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Poster;

class PosterController extends Controller
{
    public function storePoster(Request $request)
    {
        $this->validate($request, [
            'path' => 'url'
        ]);

        // Method 2
        $poster = new Poster([
            'movie_id' => $request->input('movie_id'),
            'url' => $request->input('path')
        ]);

        $poster->save();
    }

    public function updatePoster(Request $request, Poster $poster)
    {
        $this->validate($request, [
            'path' => 'url'
        ]);

        $poster->url = $request->input('path');

        $poster->update();
    }



}