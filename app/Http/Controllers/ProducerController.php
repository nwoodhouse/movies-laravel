<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producer;

class ProducerController extends Controller
{
    function index() {
    	$producers = Producer::all();
    	return view('producers.index', compact('producers'));
    }

    public function show(Producer $producer)
    {
    	return view('producers.show', compact('producer'));
    }
}
