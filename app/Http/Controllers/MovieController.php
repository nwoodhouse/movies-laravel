<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Movie;
use App\Producer;

class MovieController extends Controller
{
    public function __construct()
    {

        // ensure that the controller and policy know know the api request
        // is coming from - done via auth:api middleware
        $this->middleware('auth:api')->only('apiIndex', 'destroy');

        $this->middleware('can:search,' . Movie::class)->only('apiIndex');

        $this->middleware('can:delete,movie')->only('destroy');
    }

    public function show(Request $request, Movie $movie)
    {
        return view('movies.show', compact('movie'));
    }

    // /api/movies?search=title&order=created_at&direction=asc
    // /api/movies?search=title&order=title&direction=desc
    public function apiIndex(Request $request)
    {
        $user = $request->user();

        $searchTerm = $request->input('search');

        $query = Movie::where('title', 'LIKE', "%{$searchTerm}%");

        if ($order = $request->input('order'))
        {
            // we must sort the movies by the column specified
            $query->orderBy($order, $request->input('direction', 'asc'));
        }

        $numPerPage = $request->input('numPerPage', 10);
        $searched = $query->paginate($numPerPage);

        return response()->json($searched);
    }

    function index() {

        // profile how long it takes to do something - to see the results
        // from this, view the timeline in the Clockwork tab, in Chrome
        // dev tools
        clock()->startEvent('unique-event-id-123', "Reading from DB");

        // load movies with nested relationships
        $movies = Movie::with('producer.movies')->paginate(20);

        // stop the clock!
        clock()->endEvent('unique-event-id-123');

        return view('movies.index', compact('movies'));
    }

    public function welcome()
    {
        return view('welcome');
    }

    function saveProducer() {

        // Add a producer to a movie
        $movie = Movie::find(3);
        $newProducer = Producer::find(2);
        $movie->producer()->associate($newProducer);
        $movie->save();

        // Add a movie to a producer
        $movie5 = Movie::find(5);
        $newProducer3 = Producer::find(3);
        $newProducer3->movies()->save($movie5);


        return $movie5->title . $movie->producer->name;
    }


    function showActors() {
        $movie = Movie::find(1);
        return $movie->users;
    }

    public function destroy(Movie $movie)
    {
        $movie->delete();
        return $movie;
    }


}
