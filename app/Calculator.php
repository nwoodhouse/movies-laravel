<?php


namespace App;


class Calculator
{
    public function add($x, $y)
    {
        return $x + $y;
    }

    public function multiply($x, $y)
    {
        return $x * $y;
    }
}
