<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MoviesApiTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testExample()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function testSearchApi()
    {
        $admin = User::find(1);

        $response = $this->actingAs($admin, 'api')->getJson(route('api.movies.index') . '?search=f');

        $response->assertOk();

        $response->assertJson([
            'data' => [
                [
                    'id' => '2'
                ]
            ]
        ]);

        $response->assertSee('Forest Gump 2');
        
        $this->assertEquals($response->getStatusCode(), 200);
    }
}
