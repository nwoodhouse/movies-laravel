<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Illustrates how you can use the 'can' method on the User class to
// check if a user can do a particular action - of course, determined
// by the MoviePolicy
Route::get('test', function () {

	$user = App\User::find(1);
	$userCanDelete = $user->can('delete', App\Movie::first());
	$userCanSearch = $user->can('search', Movie::class);
	return $userCanDelete ? 'Yes' : 'No ';

});

Route::get('/', 'MovieController@welcome')->name('welcome');

Route::get('/movies', 'MovieController@index')->name('movies.index');

Route::get('/movies/{movie}', 'MovieController@show')->name('movies.show');

Route::get('/producers', 'ProducerController@index')->name('producers.index');

Route::get('/producers/{producer}', 'ProducerController@show')->name('producers.show');

Route::get('/addProducerToMovie', 'MovieController@saveProducer');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/phpinfo', 'HomeController@phpinfo')->name('phpinfo');
