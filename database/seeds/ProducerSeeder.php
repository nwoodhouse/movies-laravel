<?php

use Illuminate\Database\Seeder;
use App\Producer;

class ProducerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Producer::create([
        	'name' => 'Bob'
        ]);
    }
}
