<?php

use Illuminate\Database\Seeder;
use App\Movie;

class MoviesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {



    	// Create 1000 new Movie records using mass assignment
    	for($i=0; $i<1000; $i++)
	        Movie::create([
	        	'title' => 'Forest Gump ' . $i,
	        	'thumb' => 'http://...',
	        	'description' => 'Run forest, run!',
                'producer_id' => 1
	        ]);

        // Update a single record
        $firstMovie = Movie::find(1);
        $firstMovie->title = 'Nathan was here';
        $firstMovie->save();
    }
}
