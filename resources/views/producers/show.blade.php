@extends('master')

@section('content')
	<h1>
		Producer: {{ $producer->name }}
	</h1>

	<h2>
		The list of {{$producer->name}}'s movies is:
	</h2>

	<p>
		Movies made by this producer: {{ $producer->movies->count() }}
	</p>

	@foreach($producer->movies as $movie)
		{{ $movie->title }} <br>
	@endforeach

@endsection
