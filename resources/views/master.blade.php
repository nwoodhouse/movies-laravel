<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="api-token" content="{{ Auth::check() ? Auth::user()->api_token : '' }}">

        @yield('title')

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

        <!-- JS dependencies -->
    	<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

		<!-- For cool alerts! See: https://sweetalert2.github.io/ -->
    	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    	

    </head>
    <body>
        <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
			<a title="Home" class="navbar-brand" href="">Movies-laravel</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarText">
				<ul class="navbar-nav mr-auto">
					<li class="nav-link @if (Request::is('movies*')) active @endif">
						<a class="nav-link" href="{{ route('movies.index') }}">Movies</a>
					</li>
					<li class="nav-link @if ( Request::is('producers*')) active @endif">
						<a class="nav-link" href="{{ route('producers.index') }}">Producers</a>
					</li>
				</ul>
				
			</div>
		</nav>

		<br>
               
		<div class="container">
			@yield('content')
        <div>
    </body>
</html>
