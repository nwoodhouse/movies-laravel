@extends('master')

@section('content')
	<div id="app">
		<h1>
			Title: @{{ movie.title }}
		</h1>
		<p>
			Description: @{{ movie.description }}
		</p>

		<div v-if="selectedPoster">
			<img :src="selectedPoster.path" class="img-fluid" alt="">	
			<button class="btn btn-link" @click="selectedPoster = null">Clear</button>
		</div>
		

		<div class="row">
			<div v-for="poster in posters" class="col-4">
				<img @click="selectedPoster = poster" :src="poster.path" class="img-thumbnail img-fluid">
			</div>
		</div>
		<button @click="addPoster">Add poster</button>
		

		<button @click="incNum" class="btn" :class="getButtonClass()">
			This button has been clicked @{{ num }} times
		</button>
		<button @click="reset" class="btn btn-link" :disabled="resetDisabled">
			Reset
		</button>
	</div>
	

	<script>
		{{-- Turn the PHP $movie object, into a JSON representation --}}
		var movie = {!! $movie !!};
		var posterApp = new Vue({
			el: '#app',
			data: {
				movie: movie,
				num: 0,
				posters: [],
				selectedPoster: null
			},
			computed: {
				resetDisabled: function () {
					// if the num is 0, it means we cannot reset to anything!
					// therefore, if num is 0, then the reset is disabled
					return !this.num;
				}
			},
			methods: {

				addPoster: function () {
					var url = prompt('Enter poster URL');
					if (url)
					{
						this.posters.push({
							path: url
						});
					}
				},

				incNum: function () {
					this.num++;
				},
				reset: function () {
					this.num = 0;
				},
				getButtonClass: function () {

					var buttonClass = 'btn-light';
					if (this.num > 0 && this.num < 5)
						buttonClass = 'btn-info';
					if (this.num >= 5 && this.num < 7)
						buttonClass = 'btn-warning';
					if (this.num >= 7)
						buttonClass = 'btn-danger';

					return buttonClass;
				}
			}
		});
	</script>

@endsection
