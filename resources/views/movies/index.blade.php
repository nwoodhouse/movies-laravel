@extends('master')

@section('content')
	<H1> Movies </H1>

	{{ $movies->links() }}

	@foreach($movies as $movie)
		<strong>
			<a href="{{ route('movies.show', $movie->id) }}">{{ $movie->title }}</a>
		</strong>

		<small>
			<a href="{{ route('producers.show', $movie->producer_id) }}">{{ $movie->producer->name }}</a>

			<span class="badge badge-secondary">
				this producer has produced {{ $movie->producer->movies->count() }} movies
			</span>
		</small>

		<br>
	@endforeach
@endsection
