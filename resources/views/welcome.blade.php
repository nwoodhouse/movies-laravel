@extends('master')

@section('content')
    <h2 class="text-center">Welcome to this Movies app!</h2>
    @if(Auth::check())
        <h3 class="text-center">{{ Auth::user()->name }} - welcome back!</h3>
    @endif

    <div class="movies-search-app">
        
        <div class="row">
            <div class="col-6 offset-3">
                <div class="form-inline">
                    <div class="form-group">
                        <input v-model="search" type="text" class="form-control" placeholder="Enter search term">

                        <button v-on:click="isHidden = false" :disabled="loading == true" class="btn btn-success" @click="doSearch">Search!</button>
                    </div>
                    <div class="form-group">
                        
                        <label for="exampleFormControlSelect1">Sort by</label>
                        <select v-model="order" class="form-control" id="exampleFormControlSelect1">
                            <option v-for="column in sortableColumns">@{{ column }}</option>
                        </select>
                    </div>
                    <br>
                    <div class="form-check">
                      <input class="form-check-input" v-model=sortAsc type="checkbox" id="defaultCheck1">
                      <label class="form-check-label" for="defaultCheck1">
                        Sort asc (A-Z)
                      </label>
                    </div>

                    <div class="form-group">
                        
                        <label for="exampleFormControlSelect2">Num per page</label>
                        <select v-model="numPerPage" class="form-control" id="exampleFormControlSelect2">
                            <option v-for="n in 20">@{{ n }}</option>
                        </select>
                    </div>
                </div>
                <div v-if="loading" class="offset-4 spinner-border text-info" role="status">
                  <span class="sr-only">Loading...</span>
                </div>
                
                <div v-else class="list-group">

                    <!-- Results from the movie lookup! -->
                    <p v-if="!isHidden">Num results: <span class="badge badge-primary">@{{ numOfMovies }}</span><small> (Results @{{ start }} - @{{ end }}, page @{{ currentPage }} of @{{ lastPage }})</small></p>

                    <a v-for="movie in sortedMovies" :style="{ opacity: paging ? 0.5 : 1 }" :href="movie.url" class="list-group-item list-group-item-action">
                        @{{ movie.title }}

                        <button @click.prevent.stop="deleteMovie(movie)" class="btn btn-secondary btn-sm float-right">
                            Delete
                        </button>
                    </a>
                    <div class="row justify-content-center" v-if="!isHidden">
                        <nav aria-label="Page navigation example">
                          <ul class="pagination">
                            <li v-if="currentPage > 1" class="page-item"><button :disabled="paging" @click="prevPage" class="page-link" >Previous</button></li>
                            <li v-if="currentPage < lastPage" class="page-item"><button :disabled="paging" @click="nextPage" class="page-link" >Next</a></li>
                          </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script>

        var apiRoutes = {
            search: '{{ route('api.movies.index') }}',
            destroy: '{{ route('api.movies.destroy', '--movie--') }}'
        };

        var searchApp = new Vue({
            el: '.movies-search-app',
            data: {
                search: '',
                total: 0,
                start: 0,
                end: 0,
                movies: [],
                isHidden: true,
                numOfMovies: 0,
                currentPage: 1,
                lastPage: 0,
                loading: false,
                paging: false,

                // search criteria
                sortableColumns: [
                    'title',
                    'id',
                    'description',
                    'created_at'
                ],
                order: 'title',
                sortAsc: true,
                numPerPage: 10,

                moviesApiClient: null
            },

            mounted: function () {
                var apiToken = document.getElementsByName('api-token')[0].content;
                this.moviesApiClient = axios.create({
                    headers: {
                        Authorization: 'Bearer ' + apiToken
                    }
                });
            },

            computed: {
                sortedMovies: function () {
                    // todo - implement a sorting
                    return this.movies;
                },

            },
            methods: {             
                // lookup the set of movies that match the search term!
                // (AJAX!)
                doSearch: function () {
            
                    var that = this;

                    that.loading = true;
                    
                    // prepare the paremeters enabled by the API
                    var params = {
                        search: this.search, 
                        page: this.currentPage,
                        order: this.order,
                        direction: this.sortAsc ? 'asc' : 'desc',
                        numPerPage: this.numPerPage
                    };

                    this.moviesApiClient
                        .get(apiRoutes.search, {
                            params: params
                        })
                        .then(function (response)  {
                            // since we're using pagination...
                            that.data = response.data;
                            that.movies = response.data.data;
                            that.numOfMovies = response.data.total;
                            that.start = response.data.from;
                            that.end = response.data.to;
                            that.currentPage = response.data.current_page;
                            that.lastPage = response.data.last_page;
                            that.loading = false;
                        });

                    // var networkPromise = axios.get('/api/movies');
                    // networkPromise.then(function (response) {
                    //     console.log('here!');
                    // });
                },
                prevPage: function() {
                    var that = this;
                    that.paging = true;
                    this.moviesApiClient
                        .get('/api/movies', {params: {search: this.search, page: this.data.current_page--}})
                        .then(function (response) {
                            that.movies = response.data.data;
                            that.numOfMovies = response.data.total;
                            that.start = response.data.from;
                            that.end = response.data.to;
                            that.currentPage = response.data.current_page;
                            that.paging = false;
                        });
                },
                nextPage: function() {
                    var that = this;
                    that.paging = true;
                    this.moviesApiClient
                        .get('/api/movies', {params: {search: this.search, page: this.data.current_page++}})
                        .then(function (response) {
                            that.movies = response.data.data;
                            that.numOfMovies = response.data.total;
                            that.start = response.data.from;
                            that.end = response.data.to;
                            that.currentPage = response.data.current_page;
                            that.paging = false;
                        });
                },

                deleteMovie: function (movie) {

                    Swal.fire({
                        title: 'Are you sure?',
                        text: "Really delete this movie?",
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonText: 'Delete',
                        showLoaderOnConfirm: true
                    }).then((result) => {
                        if (result.value) {

                            var that = this;
                            var url = apiRoutes.destroy.replace('--movie--', movie.id);
                            this.moviesApiClient
                                .delete(url)
                                .then(function (response) {
                                    // item has been deleted - remove it from the local
                                    // collection of movies...
                                    var index = that.movies.indexOf(movie);
                                    that.movies.splice(index, 1);
                                    Swal.fire(
                                        'Deleted!',
                                        'Your movie has been deleted.',
                                        'success'
                                    );
                                })
                                .catch(function () {
                                    alert('Failed to delete!');
                                });

                            
                        }
                    });


                    
                }
            }
        });
    </script>
@endsection