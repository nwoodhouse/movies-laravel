Steps:

1. Clone repo and open a terminal / command prompt in the project directory (the one with the 'artisan' file in it).

2. Install the dependencies
# composer install

or
# /path/to/php /path/to/composer.phar install

3. Create the .env (from the .env.example)
	-> update the DB credentials in the .env
	-> Choose a DB_DATABASE for the schema to which the project will connect - E.g., 'movies-app'

/Applications/MAMP/bin/php7/php artisan key:generate
4. Generate SSH keys
# php artisan key:generate

or
# /path/to/php artisan key:generate

5. Go to MySQL Workbench, create a schema for the newly cloned project - E.g., 'movies-app'

6. Migrate the application
# php artisan migrate

or
# /path/to/php artisan migrate

7. Seed the DB with movies & producers
# php artisan db:seed

or
# /path/to/php artisan db:seed

8. Start the dev server (make sure you don't already another Laravel project running)
# php artisan serve

or
# /path/to/php artisan serve

9. Open browser to http://localhost:8000/movies

--------

Composer issues? See: https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos

PHP Issues? Make sure that you have a PHP 7.2 installed